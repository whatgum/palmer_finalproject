﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementManager : MonoBehaviour
{
    private bool isMoving = false;
    private string moving = "stop";

    

    public void StartWalking()
    {
        BobHead[] heads = GetComponentsInChildren<BobHead>();
        isMoving = true;
        moving = "walk";
        foreach (BobHead head in heads)
        {
            float random = Random.Range(0.001f, 0.00200f);
            head.UpdateMovementType("walk", random);
            
        }
    }

    public void StartRunning()
    {
        BobHead[] heads = GetComponentsInChildren<BobHead>();
        isMoving = true;
        moving = "run";
        foreach (BobHead head in heads)
        {
            float random = Random.Range(0.003f, 0.005f);
            head.UpdateMovementType("run",random);
            
        }
    }

    public void StartTreking()
    {
        BobHead[] heads = GetComponentsInChildren<BobHead>();
        isMoving = true;
        moving = "trek";
        foreach (BobHead head in heads)
        {
            float random = Random.Range(0.001f, 0.0025f);
            head.UpdateMovementType("trek", random);
            
        }
    }

    public void StopMoving()
    {
        BobHead[] heads = GetComponentsInChildren<BobHead>();
        isMoving = false;
        moving = "stop";
        foreach (BobHead head in heads)
        {
            float random = Random.Range(0.001f, 0.00200f);
            head.UpdateMovementType("stop", random);
            
        }
    }

    public bool IsMoving()
    {
        return isMoving;
    }
    public string Movement()
    {
        return moving;
    }
}
