﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundManager : MonoBehaviour
{
    public GameObject ground;
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "EndCollider")
        {
            Instantiate(ground, new Vector3(0f, 0f, 129.4f), Quaternion.identity);
        }
        if(other.gameObject.name == "FrontCollider")
        {
            Destroy(other.gameObject.transform.parent.gameObject);
        }
    }
    
}
