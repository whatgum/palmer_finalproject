﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public Light sun;
    public ResourceManager resources;
    public float secondsInFullDay = 120f;
    [Range(0,1)]
    public float currentTimeOfDay = 0;
    [HideInInspector]
    public float timeMultiplier = 1f;
    public MovementManager movement;
    public RandomEventManager rEvent;
    float sunInitialIntensity;
    void Start() {
        sunInitialIntensity = sun.intensity;
        UpdateSun();
        resources.ResetValues();
    }
    
    void Update()
    {
        if (Input.GetButtonDown("Quit"))
        {
            Application.Quit();
        }

        if (movement.IsMoving())
        {
            UpdateSun();
            rEvent.StartTimer();
            currentTimeOfDay += (Time.deltaTime / secondsInFullDay) * timeMultiplier;

            if (currentTimeOfDay >= 1)
            {
                currentTimeOfDay = 0;
                resources.DayUpdate();
                rEvent.LikelyhoodUpdate();
                resources.ConsumeResources();
                resources.SetWaterAndFoodSettings();
            }
        }
        else
        {
            rEvent.EndTimer();
        }
        
    }
    
    void UpdateSun() {
        sun.transform.localRotation = Quaternion.Euler((currentTimeOfDay * 360f) - 90, 170, 0);
 
        float intensityMultiplier = 1;
        if (currentTimeOfDay <= 0.23f || currentTimeOfDay >= 0.75f) {
            intensityMultiplier = 0;
        }
        else if (currentTimeOfDay <= 0.25f) {
            intensityMultiplier = Mathf.Clamp01((currentTimeOfDay - 0.23f) * (1 / 0.02f));
        }
        else if (currentTimeOfDay >= 0.73f) {
            intensityMultiplier = Mathf.Clamp01(1 - ((currentTimeOfDay - 0.73f) * (1 / 0.02f)));
        }
 
        sun.intensity = sunInitialIntensity * intensityMultiplier;
    }

}
