﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "Resources")]
public class ResourceManager : ScriptableObject
{
    private int people;
    private int food;
    private int days;
    private int water;
    private int waterSetting = 1;
    private int foodSetting = 1;
    private int newWaterSetting = 1;
    private int newFoodSetting = 1;

    public void ResetValues()
    {
        people = 1000;
        food = 80000;
        days = 0;
        water = 20000;
    }
    public void FoodUpdate(int amountBy)
    {
        food += amountBy;
    }

    public void WaterUpdate(int amountBy)
    {
        water += amountBy;
    }

    public void PeopleUpdate(int amountBy)
    {
        people += amountBy;
    }

    public void PeopleUpdate()
    {
        return;
    }
    public void FoodUpdate()
    {
        return;
    }

    public void DayUpdate()
    {
        days += 1;
        
    }
    
    public int GetDays()
    {
        return days;
    }

    public int GetPeople()
    {
        return people;
    }

    public int GetFood()
    {
        return food;
    }
    
    public int GetWater()
    {
        return water;
    }

    public void SetNewWaterSetting(int setting)
    {
        newWaterSetting = setting;
    }

    public void SetNewFoodSetting(int setting)
    {
        newFoodSetting = setting;
    }

    public int GetWaterSetting()
    {
        return waterSetting;
    }

    public int GetFoodSetting()
    {
        return foodSetting;
    }

    public int GetNewFoodSetting()
    {
        return newFoodSetting;
    }

    public int GetNewWaterSetting()
    {
        return newWaterSetting;
    }

    public void SetWaterAndFoodSettings()
    {
        waterSetting = newWaterSetting;
        foodSetting = newFoodSetting;
    }

    public void ConsumeResources()
    {
        if(foodSetting == 1)
        {
            FoodUpdate(RateOfFoodConsumption(foodSetting));
            
        }
        if(foodSetting == 2)
        {
            FoodUpdate(RateOfFoodConsumption(foodSetting));
        }
        if(foodSetting == 3)
        {
            FoodUpdate(RateOfFoodConsumption(foodSetting));
        }
        if(waterSetting == 1)
        {
            WaterUpdate(RateOfWaterConsumption(waterSetting));
        }
        if(waterSetting == 2)
        {
            WaterUpdate(RateOfWaterConsumption(waterSetting));
        }
        if(waterSetting == 3)
        {
            WaterUpdate(RateOfWaterConsumption(waterSetting));
        }
        
    }
    public int RateOfFoodConsumption(int foodSetting)
    {
        int rate = 0;
        if(foodSetting == 1)
        {
            rate = (-1 * (2 * people));
            
        }
        if(foodSetting == 2)
        {
            rate = (-1 * (4 * people));
        }
        if(foodSetting == 3)
        {
            rate =( -1 *(5 * people));
        }
        
        return rate;
    }
    public int RateOfWaterConsumption(int waterSetting)
    {
        int rate = 0;
        if(waterSetting == 1)
        {
            rate = (-1 * (people / 2));
        }
        if(waterSetting == 2)
        {
            rate = (-1 * people);
        }
        if(waterSetting == 3)
        {
            rate = (-1 * (people * 2));
        }
        return rate;
    }
}
