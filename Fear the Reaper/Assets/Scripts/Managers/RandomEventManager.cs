﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomEventManager : MonoBehaviour
{
    public Tribe tribe;
    private List<Event> arrayOfEvents = new List<Event>();
    private float chance = 0.05f;
    private System.DateTime startTime;
    private bool movement = false;
    private bool sicknessTried = false;
    private void Awake()
    {
        //loading all gameobjects from events folder into the game
        GameObject[] objects = Resources.LoadAll<GameObject>("Prefabs/Events");
        

        //get the event components from each gameobject
        foreach (GameObject obj in objects)
        {
            arrayOfEvents.Add(obj.GetComponent<Event>());
        }

        //start the timer
        startTime = System.DateTime.UtcNow;
    }

    private void FixedUpdate()
    {
        if(movement == true)
        {
            System.TimeSpan ts = System.DateTime.UtcNow - startTime;

            if(ts.Seconds == 5 && sicknessTried == false)
            {
                for(int i = 0; i < 3; i++)
                {
                    tribe.TrySickness();
                }
                sicknessTried = true;
            }
            
            if (ts.Seconds >= 10)
            {
                startTime = System.DateTime.UtcNow;
                sicknessTried = false;
                TryRandomEvent();
            }
            
        }
        
    }

    public void LikelyhoodUpdate()
    {
        chance += .05f;
    }

    private void TryRandomEvent()
    {
        float rollChance = Random.Range(0f, 1f);
        float outcome = rollChance + chance;
        Debug.Log(outcome);
        if(outcome >= 1)
        {
            int randomEventIndex = Random.Range(0, arrayOfEvents.Count - 1);
            Event randomEvent = arrayOfEvents[randomEventIndex];
            randomEvent.Init();
            gameObject.GetComponentInParent<MovementManager>().StopMoving();
            chance = .05f;
        }
    }
    public void StartTimer()
    {
        if(movement == true)
        {
            return;
        }
        startTime = System.DateTime.UtcNow;
        movement = true;
    }
    public void EndTimer()
    {
        movement = false;
    }
}
