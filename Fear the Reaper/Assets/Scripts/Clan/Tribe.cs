﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tribe : MonoBehaviour
{
    List<Tribeman> tribe = new List<Tribeman>();
    public ResourceManager resources;
    private MovementManager movementManager;
    private int people;
    private void Awake()
    {
        Tribeman[] array = gameObject.GetComponentsInChildren<Tribeman>();
        foreach(Tribeman t in array)
        {
            tribe.Add(t);
        }
        people = 1000;
        movementManager = gameObject.GetComponentInParent<MovementManager>();
    }
    private void Update()
    {
        if(people != resources.GetPeople())
        {
            PeopleDie(people - resources.GetPeople());
            people = resources.GetPeople();
        }
        if (Input.GetMouseButtonDown(0) && movementManager.IsMoving())
        { // if left button pressed...
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            //if the click is on a object in the game
            if (Physics.Raycast(ray, out hit, 1000))
            {
                if(hit.transform != null)
                {
                    if(hit.transform.gameObject.transform.parent != null)
                    {
                        Tribeman tryTribe1 = hit.transform.gameObject.transform.parent.transform.GetComponentInParent<Tribeman>();
                        if(tryTribe1 != null)
                        {
                            if(tryTribe1.IsSick() == true)
                            {
                                tryTribe1.GetSaved();
                            }
                        }
                        
                        if(hit.transform.gameObject.transform.parent.transform.parent != null)
                        {
                            Tribeman tryTribe2 = hit.transform.gameObject.transform.parent.transform.parent.GetComponentInParent<Tribeman>();
                            if(tryTribe2 != null)
                            {
                                if(tryTribe2.IsSick() == true)
                                {
                                    tryTribe2.GetSaved();
                                }
                            }
                        }
  
                    }
                    

                }
                
                
            }
        }
        SickPeopleDie();

    }

    private void PeopleDie(int amount)
    {
        for(int i = 0; i < amount; i++)
        {
            if(tribe.Count != 0)
            {
                int index = Random.Range(0, tribe.Count);
                
                tribe[index].Die();
                tribe.RemoveAt(index);
            }
            
        }
    }

    public void TrySickness()
    {
        int population = 1;
        string movement = movementManager.Movement();
        if(movement.Equals("run"))
        {
            population = 10;
        }
        if(movement.Equals("trek"))
        {
            population = 5;
        }
        
        float oddsToBeat = .1f + (resources.GetWaterSetting() / 10) +  (resources.GetFoodSetting() / 10);

        float gamble = Random.Range(0f, 1f);

        if(oddsToBeat <= gamble)
        {
            GetSick(population);
        }
        
    }

    private void GetSick(int amount)
    {
        List<int> listOfInt = new List<int>();
        for(int i = 0; i < amount; i++)
        {
            if(tribe.Count != 0)
            {
                int index = Random.Range(0, tribe.Count);
                while(listOfInt.Contains(index))
                {
                    index = Random.Range(0, tribe.Count);
                }

                listOfInt.Add(index);
                tribe[index].GetSick();
                
            }
            
        }
        
    }
    public void SickPeopleDie()
    {
        int amount = 0;
        for(int i = 0; i < tribe.Count;i++)
        {
            if(tribe[i] == null)
            {
                tribe.RemoveAt(i);
                amount -= 1;
            }
            else if(tribe[i].IsSick() == true && tribe[i].IsAlive() == false)
            {
                if(tribe[i].Counted() == false)
                {
                    tribe[i].Count();
                    amount -= 1;
                    
                }
                
                
            }
            
        }
        resources.PeopleUpdate(amount);
        people += amount;
    }

}
