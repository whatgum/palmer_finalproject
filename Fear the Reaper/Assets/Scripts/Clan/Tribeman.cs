﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tribeman : MonoBehaviour
{
    private BobHead bobHead;
    private bool alive = true;
    private bool dieOfSickness = true;
    private bool sick = false;
    private int timeTillDeath = 7;
    private System.DateTime startTime;
    private int secondsLeftIfTimeStopped;
    private bool firstTimeStopped = true;
    public bool firstTimeDied = true;
    private Vector3 ogPosition;
    private Color originalColor;
    private bool reachedHeaven = false;
    private bool counted = false;
    private string movement = "stop";
    private float randomMovement;
    public void Awake()
    {
        bobHead = gameObject.GetComponent<BobHead>();
        originalColor = gameObject.transform.GetChild(1).transform.GetChild(0).GetComponent<Renderer>().material.color;
        ogPosition = gameObject.transform.position;
        randomMovement = Random.Range(0.04f, 0.100f);
    }

    public void Die()
    {
        alive = false;
    }

    private void Update()
    {
        if(alive == false)
        {
            if(reachedHeaven == false)
            {
                if(bobHead.GetMovementType().Equals("stop") == false)
                {
                    movement = bobHead.GetMovementType();
                    bobHead.UpdateMovementType("stop", 0);
                }
                gameObject.transform.position += new Vector3(0f, .05f, 0f);
            }
            
            if(gameObject.transform.position.y >= 7)
            {
                reachedHeaven = true;
                if(firstTimeDied == false)
                {
                    Destroy(gameObject);
                }
                else
                {
                    gameObject.transform.position = new Vector3(ogPosition.x, ogPosition.y, -22.5f);
                    firstTimeDied = false;
                    gameObject.transform.GetChild(1).transform.GetChild(0).GetComponent<Renderer>().material.color = originalColor;
                    if(movement.Equals("walk"))
                    {
                        float random = Random.Range(0.001f, 0.00200f);
                        bobHead.UpdateMovementType("walk", random);
                    }
                    if(movement.Equals("trek"))
                    {
                        float random = Random.Range(0.001f, 0.0025f);
                        bobHead.UpdateMovementType("trek", random);
                    }

                    if(movement.Equals("run"))
                    {
                        float random = Random.Range(0.003f, 0.005f);
                        bobHead.UpdateMovementType("run",random);
                    }
                    
                }
            }
            else if(gameObject.transform.position.z < ogPosition.z)
            {
                gameObject.transform.position += new Vector3(0f, 0f, randomMovement);
            }
            else if(gameObject.transform.position.z > ogPosition.z)
            {
                gameObject.transform.position = ogPosition;
                reachedHeaven = false;
                alive = true;
                sick = false;
                counted = false;
                secondsLeftIfTimeStopped = 0;
            }
            
            
        }
        if(sick == true)
        {
            System.TimeSpan ts = System.DateTime.UtcNow - startTime;

            if(ts.Seconds + secondsLeftIfTimeStopped >= timeTillDeath && !bobHead.movementType.Equals("stop"))
            {
                MaybeDie();
            }
            if(bobHead.movementType.Equals("stop"))
            {
                if(firstTimeStopped == true)
                {
                    firstTimeStopped = false;
                    secondsLeftIfTimeStopped += ts.Seconds;
                }
                startTime = System.DateTime.UtcNow;
            }
            else
            {
                firstTimeStopped = true;

            }
        }
    }

    public void GetSick()
    {
        gameObject.transform.GetChild(1).transform.GetChild(0).GetComponent<Renderer>().material.color = Color.green;
        sick = true;
        startTime = System.DateTime.UtcNow;
    }

    private void MaybeDie()
    {
        if (dieOfSickness == true)
        {
            Die();
            secondsLeftIfTimeStopped = 0;
        }
        
        if(dieOfSickness == false)
        {
            sick = false;
            dieOfSickness = true;
            secondsLeftIfTimeStopped = 0;
        }
    }
    public void GetSaved()
    {
        if(alive == true)
        {
            dieOfSickness = false;
            gameObject.transform.GetChild(1).transform.GetChild(0).GetComponent<Renderer>().material.color = originalColor;
        }
        
    }
    public bool IsSick()
    {
        return sick;
    }
    public bool IsAlive()
    {
        return alive;
    }
    public bool Counted()
    {
        return counted;
    }
    public void Count()
    {
        counted = true;
    }
    
}
