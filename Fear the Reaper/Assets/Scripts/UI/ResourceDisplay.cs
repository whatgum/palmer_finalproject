﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourceDisplay : MonoBehaviour
{
    public Text foodText;
    public Text waterText;
    public Text foodConsumptionText;
    public Text waterConsumptionText;
    public ResourceManager resourceManager;

    private void Update()
    {
        UpdateFoodText();
        UpdateWaterText();
        UpdateFoodConsumptionText();
        UpdateWaterConsumptionText();
    }

    private void UpdateWaterText()
    {
        waterText.text = "Gallons of water: " + resourceManager.GetWater();
    }

    private void UpdateFoodText()
    {
        foodText.text = "Pounds of food: " + resourceManager.GetFood();
    }
    
    private void UpdateFoodConsumptionText()
    {
        int consumption = -1*resourceManager.RateOfFoodConsumption(resourceManager.GetNewFoodSetting());
        foodConsumptionText.text = "Consuming " + consumption+ " pounds per day \n" + resourceManager.GetFood()/consumption+ " days of food left";
        
        
    }

    private void UpdateWaterConsumptionText()
    {
        int consumption = -1*resourceManager.RateOfWaterConsumption(resourceManager.GetNewWaterSetting());

            waterConsumptionText.text = "Consuming " + consumption + " gallons per day \n" + resourceManager.GetWater()/consumption + " days of water left";
        
    }

    public void ChangeWaterSetting(int setting)
    {
        resourceManager.SetNewWaterSetting(setting);
    }
    
    public void ChangeFoodSetting(int setting)
    {
        resourceManager.SetNewFoodSetting(setting);
    }
}
