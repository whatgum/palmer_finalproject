﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuSwitcher : MonoBehaviour
{
    string whichMenu = "Prophet";
    public GameObject clan;
    public CanvasGroup mainScreen;
    public CanvasGroup prophetScreen;
    public CanvasGroup mapScreen;
    public CanvasGroup resourcesScreen;
    private MovementManager movement;
    Vector3 oldPosition = new Vector3(0f, 9.24f, -21.59f);    
    Vector3 cameraNewRotation = new Vector3(0f, 180f, 0f);
    Vector3 firstTransitionPosition = new Vector3(0f, 9.24f, -0.843f);
    Vector3 secondTransitionPosition = new Vector3(0.128f, 1.362f, -0.843f);

    bool transition = false;
    float rotationAmountX = 0f;
    float rotationAmountY = 0f;
    private void Awake()
    {
        movement = gameObject.GetComponentInParent<MovementManager>();
    }

    void Update()
    {

        

        if (Input.GetMouseButtonDown(0))
        { // if left button pressed...
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            //if the click is on a object in the game
            if (Physics.Raycast(ray, out hit, 1000))
            {
                if(hit.transform != null)
                {
                    if(hit.transform.tag == "Prophet")
                    {
                        transition = true;
                    }
                    
                }
                
                
            }
        }

        if (transition == true && whichMenu == "Main")
        {
            MoveCameraToProphetMenu();
            movement.StopMoving();
        }
        if (transition == true && whichMenu == "Prophet")
        {
            MoveCameraToMainMenu();
        }
        
    }

    

    void MoveCameraToProphetMenu()
    {
        Debug.Log(rotationAmountY + " " + rotationAmountX);
        Vector3 newPosition = firstTransitionPosition;
        mainScreen.interactable = false;
        mainScreen.alpha = 0;
        mainScreen.blocksRaycasts = false;
        if(Camera.main.transform.position.z == firstTransitionPosition.z)
        {
            newPosition = secondTransitionPosition;
        
            if(rotationAmountX != -50f)
            {
                Camera.main.transform.Rotate(-1f, 0f, 0f);
                rotationAmountX += -1f;
            }
            if(rotationAmountX == -50 && rotationAmountY <= 180)
            {
                Camera.main.transform.Rotate(0f, 2f, 0f);
                rotationAmountY += 2f;
            }
            
        }

        if(Camera.main.transform.position == secondTransitionPosition)
        {
            if(rotationAmountX == -50 && rotationAmountY <= 180)
            {
                Camera.main.transform.Rotate(0f, 1f, 0f);
                rotationAmountY += 1f;
            }
            else
            {
                prophetScreen.interactable = true;
                prophetScreen.alpha = 1;
                prophetScreen.blocksRaycasts = true;
                transition = false;
                whichMenu = "Prophet";
                rotationAmountY = 0;
                rotationAmountX = 0;
                Debug.Log("transition ended");
               
            }

        }

        Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, newPosition, Time.deltaTime * 2);
        
        if (Camera.main.transform.position.z < -.84f && Camera.main.transform.position.z > -1f && newPosition == firstTransitionPosition)
        {
            Camera.main.transform.position = firstTransitionPosition;
        }
        if (Camera.main.transform.position.x > .127f)
        {
            Camera.main.transform.position = secondTransitionPosition;
        }
    }

    public void SwitchToMapMenu()
    {
        prophetScreen.alpha = 0;
        prophetScreen.interactable = false;
        mapScreen.interactable = true;
        mapScreen.alpha = 1;
        mapScreen.blocksRaycasts = true;
        clan.SetActive(false);
    }
    public void SwitchToProphetFromMap()
    {
        prophetScreen.alpha = 1;
        prophetScreen.interactable = true;
        mapScreen.interactable = false;
        mapScreen.alpha = 0;
        mapScreen.blocksRaycasts = false;
        clan.SetActive(true);
    
    }

    public void SwitchToResourcesMenu()
    {
        prophetScreen.alpha = 0;
        prophetScreen.interactable = false;
        resourcesScreen.interactable = true;
        resourcesScreen.alpha = 1;
        resourcesScreen.blocksRaycasts = true;
        clan.SetActive(false);
    }

    public void SwitchToProphetFromResources()
    {
        prophetScreen.alpha = 1;
        prophetScreen.interactable = true;
        resourcesScreen.interactable = false;
        resourcesScreen.alpha = 0;
        resourcesScreen.blocksRaycasts = false;
        clan.SetActive(true);
    }

    void MoveCameraToMainMenu()
    {
        Vector3 newPosition = firstTransitionPosition;
        prophetScreen.interactable = false;
        prophetScreen.alpha = 0;
        prophetScreen.blocksRaycasts = false;
        if(Camera.main.transform.position.z == firstTransitionPosition.z)
        {
        
            if(rotationAmountY != -180)
            {
                Camera.main.transform.Rotate(0f, -20f, 0f);
                rotationAmountY += -20f;
            }
            if(rotationAmountY == -180 && rotationAmountX != 50)
            {
                Camera.main.transform.Rotate(1f, 0f, 0f);
                rotationAmountX += 1f;
            }
            
        }

        if(Camera.main.transform.position.y >= 9)
        {
            newPosition = oldPosition;
            
        }

        if(Camera.main.transform.position == oldPosition)
        {
            mainScreen.interactable = true;
            mainScreen.alpha = 1;
            mainScreen.blocksRaycasts = true;
            transition = false;
            whichMenu = "Main";
            rotationAmountY = 0;
            rotationAmountX = 0;
            Debug.Log("transition ended");
        }

        Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, newPosition, Time.deltaTime * 2);

        if(Camera.main.transform.position.z < -21.57f)
        {
            Camera.main.transform.position = oldPosition;
        }
    }
    public void SwitchMenu()
    {
        transition = true;
    }
    public string WhichMenu()
    {
        return whichMenu;
    }
    public void ChangeToProphetMenuQuickly()
    {
        Camera.main.transform.Rotate(-50f, 0f, 0f);
        Camera.main.transform.position = new Vector3(.128f, 1.362f, -.843f);
        Camera.main.transform.Rotate(0f, -180f, 0f);
        whichMenu = "Prophet";
        mainScreen.interactable = false;
        mainScreen.alpha = 0;
    }
}
