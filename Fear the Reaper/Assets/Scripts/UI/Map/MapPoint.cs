﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapPoint : MonoBehaviour
{
    public float distance = 100f;
    private float distanceTraveled = 0;
    private bool reachedHere = false;
    public bool choosen = false;
    private float distanceTraveledPerSecond = .1f;
    public Event reachedHereEvent;
    Slider slider;

    public void Choosen()
    {
        choosen = true;
        slider = gameObject.GetComponentInChildren<Slider>();
        
    }
    
    public void Update()
    {
        if(choosen == true)
        {
            slider = gameObject.GetComponentInChildren<Slider>();
            string movement = gameObject.GetComponentInParent<Map>().Movement();
            if(movement.Equals("stop") == false && movement.Equals("") == false)
            {
                ChangeDistanceTraveledPerSecond(movement);
               
                distanceTraveled += distanceTraveledPerSecond;
                ChangeSliderValue(distanceTraveled);
                
            }
        }
        if(reachedHere == true)
        {
            reachedHere = false;
            ShowConsequence();
        }
    }

    private void ShowConsequence()
    {
        gameObject.GetComponentInParent<Map>().GoToProphetThenMap();
        reachedHereEvent.Init();
        gameObject.GetComponentInParent<Map>().ChooseNewPlace();
    }

    private void ChangeDistanceTraveledPerSecond(string movement)
    {
        
        if(movement.Equals("walk"))
        {
            distanceTraveledPerSecond = .1f;
        }

        if(movement.Equals("trek"))
        {
            distanceTraveledPerSecond = .15f;
        }
        
        if(movement.Equals("run"))
        {
            distanceTraveledPerSecond = .2f;
        }
        
    }

    private void ChangeSliderValue(float distanceTraveled)
    {
        float percentageTraveled = distanceTraveled / distance;
       
        if (percentageTraveled >= 1)
        {
            percentageTraveled = 1f;
            reachedHere = true;
            choosen = false;
        }

        slider.value = percentageTraveled;
    }
    
}
