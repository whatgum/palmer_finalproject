﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Map : MonoBehaviour
{
    public MovementManager movementManager;
    public MenuSwitcher menuSwitcher;
    public EndGame endGame;
    private string movement;
    private List<GameObject> mapPoints = new List<GameObject>();
    private GameObject gam;
    private string from = "";

    void Awake()
    {
        movement = movementManager.Movement();
        
        foreach (Transform child in transform)
        {  
            mapPoints.Add(child.gameObject);
        }

    }

    void Update()
    {
        movement = movementManager.Movement();
    }

    public void GoToProphetThenMap()
    {
        movementManager.StopMoving();
        string menu = menuSwitcher.WhichMenu();

        if(menu.Equals("Main"))
        {
            menuSwitcher.ChangeToProphetMenuQuickly();
            menuSwitcher.SwitchToMapMenu();
        }
        else
        {
            menuSwitcher.SwitchToMapMenu();
        }

    }

    public void NewPlaceToGo(string goingTo)
    {
        foreach (GameObject point in mapPoints)
        {

            if(point.name == goingTo)
            {

                if(goingTo.Equals("Shim City"))
                {

                    if(from.Equals("Flinbin Springs"))
                    {
                        Destroy(point.transform.GetChild(0).gameObject);
                    }

                }

                else
                {
                    Destroy(point.transform.GetChild(1).gameObject);
                }

                point.GetComponent<MapPoint>().Choosen();
                from = goingTo;
            }
            if(point.name != "Button")
            {
                point.GetComponentInChildren<Button>().interactable = false;
            }
            if(point.name == "Button")
            {
                point.GetComponent<Button>().interactable = true;
            }
        }
    }

    public void ChooseNewPlace()
    {
        int index = 0;
        int indexOfFrom = 0;
        foreach (GameObject point in mapPoints)
        {
            if(point.name == from)
            {
                indexOfFrom = index;
            }
 
            if(point.name == "Button")
            {
                point.GetComponent<Button>().interactable = false;
            }
 
            index++;
        }
        if(indexOfFrom == 6)
        {
            mapPoints[indexOfFrom-1].GetComponentInChildren<Button>().interactable = true;
            mapPoints[indexOfFrom-2].GetComponentInChildren<Button>().interactable = true;
        }
        else if(indexOfFrom == 5)
        {
            mapPoints[indexOfFrom-3].GetComponentInChildren<Button>().interactable = true;
        }
        else if(indexOfFrom == 3)
        {
            mapPoints[indexOfFrom-2].GetComponentInChildren<Button>().interactable = true;
        }
        else if(indexOfFrom == 0)
        {
            mapPoints[indexOfFrom].GetComponentInChildren<Button>().interactable = false;
            endGame.EndTheGame(1);
        }
        else
        {
            mapPoints[indexOfFrom-1].GetComponentInChildren<Button>().interactable = true;
        }
        
    }

    public string Movement()
    {
        return movement;
    }
 
}
