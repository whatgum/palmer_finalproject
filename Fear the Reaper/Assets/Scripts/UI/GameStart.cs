﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SA
{
    //this script is for the beginning canvas to tell the player how to play
    public class GameStart : MonoBehaviour
    {
        private int numOfText = 10;
        public GameObject text;
        private bool removeCanvas = false;
        public CanvasGroup canvas;
        private float m_Timer;
        public float fadeDuration = 1f;

        //turn the text to the next
        public void turnText()
        {
            switch (numOfText)
            {
                case 10:
                    text.GetComponent<Text>().text = "Long ago, the people of this valley flourished.";
                    break;

                case 9:
                    text.GetComponent<Text>().text = "Now all that's left is the shifting sands of loose soil.";
                    break;

                case 8:
                    text.GetComponent<Text>().text = "You must bring your people west for it was told to you by God that there exists a great promised land.";
                    break;

                case 7:
                    text.GetComponent<Text>().text = "You are a prophet and a leader to the 1000 members of your clan.";
                    break;

                case 6:
                    text.GetComponent<Text>().text = "Not everyone will make it and you must make decisions that will lead men to their deaths.";
                    break;

                case 5:
                    text.GetComponent<Text>().text = "Keep track of your resources and your people's wellbeing.";
                    break;
              
                case 4:
                    text.GetComponent<Text>().text = "If someone goes green, it means they are sick. Click on them to take care of them before they die.";
                    break;
                    
                case 3:
                    text.GetComponent<Text>().text = "The journey will be long and walking will make sure you'll get there safely, speeding up travel will increase the speed of travel but also the danger.";
                    break;

                case 2:
                    text.GetComponent<Text>().text = "To navigate between the prophet menu and the main screen, click on yourself.";
                    break;

                case 1:
                    text.GetComponent<Text>().text = "To begin your journey, click on Animo City on the map.";
                    break;

                //once we hit the end remove this canvas, make the ui usable and start game
                default:
                    removeCanvas = true;
                    canvas.interactable = false;
                    break;
            }

            numOfText = numOfText - 1;
        }

        private void Update()
        {
            if (removeCanvas == true)
            {
                RemoveCanvas();
            }
        }
        //function to remove this canvas
        private void RemoveCanvas()
        {
            m_Timer += Time.deltaTime;
            float fade = m_Timer / fadeDuration;
            canvas.alpha -= fade;
            if (canvas.alpha <= 0f)
            {
                removeCanvas = false;
                Transform transform = gameObject.transform;
                foreach (Transform child in transform)
                {
                    Destroy(child.gameObject);
                }
                Destroy(gameObject);
            }
        }
    }
}
