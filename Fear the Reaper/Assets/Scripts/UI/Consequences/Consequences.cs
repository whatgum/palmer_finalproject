﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Consequences : MonoBehaviour
{
    public Consequences obj;
    private GameObject canvas;
    public void Init()
    {
        canvas = GameObject.FindGameObjectWithTag("Canvas");
        Instantiate(obj, canvas.transform, false);
    }

    public void RemoveSelf()
    {
        Destroy(GameObject.FindGameObjectWithTag("Canvas").transform.GetChild(GameObject.FindGameObjectWithTag("Canvas").transform.childCount - 1).gameObject);
        if(GameObject.FindGameObjectWithTag("Canvas").transform.GetChild(0).GetComponent<CanvasGroup>().alpha == 1)
        {
            GameObject.FindGameObjectWithTag("Canvas").transform.GetChild(0).GetComponent<CanvasGroup>().interactable = true;
        }
        if(GameObject.FindGameObjectWithTag("Canvas").transform.GetChild(1).gameObject.GetComponent<CanvasGroup>().alpha == 1)
        {
            GameObject.FindGameObjectWithTag("Canvas").transform.GetChild(1).gameObject.GetComponent<CanvasGroup>().interactable = true;
        }
        if (GameObject.FindGameObjectWithTag("Canvas").transform.GetChild(2).gameObject.GetComponent<CanvasGroup>().alpha == 1)
        {
            GameObject.FindGameObjectWithTag("Canvas").transform.GetChild(2).gameObject.GetComponent<CanvasGroup>().interactable = true;
        }
        if (GameObject.FindGameObjectWithTag("Canvas").transform.GetChild(3).gameObject.GetComponent<CanvasGroup>().alpha == 1)
        {
            GameObject.FindGameObjectWithTag("Canvas").transform.GetChild(3).gameObject.GetComponent<CanvasGroup>().interactable = true;
        }
    }
}
