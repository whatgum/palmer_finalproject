﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EndGame : MonoBehaviour
{
    public ResourceManager resources;
    private CanvasGroup canvas;
    private Text text;
    private bool firstTime = true;
    private void Awake()
    {
        text = gameObject.GetComponentInChildren<Text>();
        canvas = gameObject.GetComponent<CanvasGroup>();
    }
    private void Update()
    {
        if ((resources.GetFood() <= 0 || resources.GetWater() <= 0) && firstTime == true)
        {
            firstTime = false;
            EndTheGame(0);
        }
        
    }
    public void EndTheGame(int ending)
    {
        if (ending == 0)
        {
            text.text = "You ran out of resources and sucummed to the wastes";
        }
        if(ending == 1)
        {
            text.text = "You managed to make it to the promised land but does the blood on your hands justify your sucess?";
        }
        canvas.alpha = 1;
    }
}
