﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MainScreen : MonoBehaviour
{
    public ResourceManager resources;
    public Text waterLeft;
    public Text foodLeft;
    public Text peopleLeft;
    public Text days;
    // Update is called once per frame
    void Update()
    {
        UpdateFoodConsumptionText();
        UpdateWaterConsumptionText();
        UpdatePeopleText();
        UpdateDayText();
    }
    
    private void UpdatePeopleText()
    {
        peopleLeft.text = "Pilgrims: " + resources.GetPeople();
    }
    private void UpdateDayText()
    {
        days.text = "Days: " + resources.GetDays();
    }
    private void UpdateFoodConsumptionText()
    {
        int consumption = -1*resources.RateOfFoodConsumption(resources.GetNewFoodSetting());
        foodLeft.text = "Days till food runs out: " + resources.GetFood()/consumption; 
    }

    private void UpdateWaterConsumptionText()
    {
        int consumption = -1*resources.RateOfWaterConsumption(resources.GetNewWaterSetting());

        waterLeft.text = "Days till water runs out: "  + resources.GetWater()/consumption;
    }
}
