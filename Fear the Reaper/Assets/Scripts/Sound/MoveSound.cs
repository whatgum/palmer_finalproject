﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSound : MonoBehaviour
{
    private AudioSource audioOut;
    private float maxHeight = .1f;
    private float walkIncrement = .0001f;
    private float runIncrement = .001f;
    private float trekIncrement = .0006f;
    private float randomIncrement;
    private string movementType = "stop";
    private float amount = 0f;
    private void Awake()
    {
        audioOut = gameObject.GetComponent<AudioSource>();
        float pitch = Random.Range(0.97f, .99f);
        float volume = Random.Range(.10f, .30f);
        int priority = Random.Range(1, 200);
        audioOut.pitch = pitch;
        audioOut.volume = volume;
        audioOut.priority = priority;
    }

    private void Update()
    {
        if (movementType == "walk")
        {
            if(amount >= maxHeight)
            {
                if(walkIncrement > 0)
                {
                    walkIncrement = walkIncrement * -1;
                    
                }
                if (randomIncrement > 0)
                {
                    randomIncrement = randomIncrement * -1;
                }
                

            }

            if (amount <= 0)
            {
                if(walkIncrement < 0)
                {
                    walkIncrement = walkIncrement * -1;
                    
                    
                }
                if(randomIncrement < 0)
                {
                    randomIncrement = randomIncrement * -1;
                }
               
                audioOut.PlayDelayed(Random.Range(0f,.03f));
                
            }

            amount += walkIncrement + randomIncrement;
        }

        if (movementType == "trek")
        {
            if (amount >= maxHeight)
            {
                if (trekIncrement > 0)
                {
                    trekIncrement = trekIncrement * -1;

                }
                if (randomIncrement > 0)
                {
                    randomIncrement = randomIncrement * -1;
                }
            }

            if (amount <= 0)
            {
                if (runIncrement < 0)
                {
                    trekIncrement = trekIncrement * -1;

                }
                if (randomIncrement < 0)
                {
                    randomIncrement = randomIncrement * -1;
                }
                audioOut.PlayDelayed(Random.Range(0f,.03f));
                
            }

            amount += trekIncrement + randomIncrement;
        }

        if (movementType == "run")
        {
            if(amount >= maxHeight)
            {
                if(runIncrement > 0)
                {
                    runIncrement = runIncrement * -1;
                    
                }
                if (randomIncrement > 0)
                {
                    randomIncrement = randomIncrement * -1;
                }
            }

            if(amount <= 0)
            {
                if(runIncrement < 0)
                {
                    runIncrement = runIncrement * -1;
                    
                }
                if (randomIncrement < 0)
                {
                    randomIncrement = randomIncrement * -1;
                }
                
                audioOut.PlayDelayed(Random.Range(0f,.03f));
                
            }

            amount += runIncrement + randomIncrement;
        }

        if (movementType == "stop")
        {
            if(gameObject.transform.position.y != 0)
            {
                if(gameObject.transform.position.y > 0)
                {
                    gameObject.transform.position -= new Vector3(0f, .001f, 0f);
                }
                if(gameObject.transform.position.y < 0)
                {
                    gameObject.transform.position += new Vector3(0f, .001f, 0f);
                }
            }
            
        }
    }

    public void UpdateMoveSound(string movement, float random)
    {
        movementType = movement;
        randomIncrement = random;
    }

}
