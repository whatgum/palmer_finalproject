﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BobHead : MonoBehaviour
{
    private float maxHeight = .1f;
    public string movementType = "stop";
    public float walkIncrement = .0001f;
    private float runIncrement = .001f;
    public float randomIncrement;
    private float trekIncrement = .0006f;
    public string GetMovementType()
    {
        return movementType;
    } 
    public void Update()
    {
        if (movementType == "walk")
        {
            if(gameObject.transform.position.y >= maxHeight)
            {
                if(walkIncrement > 0)
                {
                    walkIncrement = walkIncrement * -1;
                    
                }
                if (randomIncrement > 0)
                {
                    randomIncrement = randomIncrement * -1;
                }


            }

            if (gameObject.transform.position.y <= 0)
            {
                if(walkIncrement < 0)
                {
                    walkIncrement = walkIncrement * -1;
                    
                    
                }
                if(randomIncrement < 0)
                {
                    randomIncrement = randomIncrement * -1;
                }
            }

            gameObject.transform.position += new Vector3(0f, walkIncrement + randomIncrement, 0f);
        }

        if (movementType == "trek")
        {
            if (gameObject.transform.position.y >= maxHeight)
            {
                if (trekIncrement > 0)
                {
                    trekIncrement = trekIncrement * -1;

                }
                if (randomIncrement > 0)
                {
                    randomIncrement = randomIncrement * -1;
                }
            }

            if (gameObject.transform.position.y <= 0)
            {
                if (runIncrement < 0)
                {
                    trekIncrement = trekIncrement * -1;

                }
                if (randomIncrement < 0)
                {
                    randomIncrement = randomIncrement * -1;
                }
            }

            gameObject.transform.position += new Vector3(0f, trekIncrement + randomIncrement, 0f);
        }

        if (movementType == "run")
        {
            if(gameObject.transform.position.y >= maxHeight)
            {
                if(runIncrement > 0)
                {
                    runIncrement = runIncrement * -1;
                    
                }
                if (randomIncrement > 0)
                {
                    randomIncrement = randomIncrement * -1;
                }
            }

            if(gameObject.transform.position.y <= 0)
            {
                if(runIncrement < 0)
                {
                    runIncrement = runIncrement * -1;
                    
                }
                if (randomIncrement < 0)
                {
                    randomIncrement = randomIncrement * -1;
                }
            }

            gameObject.transform.position += new Vector3(0f, runIncrement + randomIncrement, 0f);
        }

        if (movementType == "stop")
        {
            if(gameObject.transform.position.y != 0)
            {
                if(gameObject.transform.position.y > 0)
                {
                    gameObject.transform.position -= new Vector3(0f, .001f, 0f);
                }
                if(gameObject.transform.position.y < 0)
                {
                    gameObject.transform.position += new Vector3(0f, .001f, 0f);
                }
            }
        }
    }

    public void UpdateMovementType(string movement, float random)
    {
        MoveArm[] arms = GetComponentsInChildren<MoveArm>();
        MoveSound sound = gameObject.GetComponent<MoveSound>();
        movementType = movement;
        randomIncrement = random;
        foreach (MoveArm arm in arms)
        {
            arm.UpdateMovmenet(movement, random);
        }
        sound.UpdateMoveSound(movement, random);
    }
   
}
