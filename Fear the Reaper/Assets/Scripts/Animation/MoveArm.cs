﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveArm : MonoBehaviour
{
    float rotationXMax = 15f;
    float rotationXMin = -15f;
    public string direction;
    public string movementType = "stop";
    private float movement;
    private float random;
    private void Update()
    {
        
        if (movementType == "walk")
        {
            if (direction == "forward")
            {
                gameObject.transform.Rotate(-.50f + (random * -1), 0f, 0f);
                movement += -.50f + (random * -1);
                if (rotationXMin >= movement)
                {
                    direction = "backward";
                }
            }
            if (direction == "backward")
            {
                gameObject.transform.Rotate(.50f + random, 0f, 0f);
                movement += .50f + random;
                if (rotationXMax <= movement)
                {
                    direction = "forward";
                }
            }
        }

        if (movementType == "run")
        {
            if (direction == "forward")
            {
                gameObject.transform.Rotate(-1.5f + (random * -1), 0f, 0f);
                movement += -1.5f + (random * -1);
                if (rotationXMin >= movement)
                {
                    direction = "backward";
                }
            }
            if (direction == "backward")
            {
                gameObject.transform.Rotate(1.5f + random, 0f, 0f);
                movement += 1.5f + random;
                if (rotationXMax <= movement)
                {
                    direction = "forward";
                }
            }
        }
        if (movementType == "stop")
        {
            if (movement != 0)
            {
                if(movement > 0)
                {
                    gameObject.transform.Rotate(-.001f, 0f, 0f);
                    movement += -.001f;
                }
                if(movement < 0)
                {
                    gameObject.transform.Rotate(.001f, 0f, 0f);
                    movement += .001f;
                }
            }
        }
    }

    public void UpdateMovmenet(string movement, float random)
    {
        movementType = movement;
        this.random = random;
    }


}
